# oci-proto-build

oci-proto-build is a Docker container containing all required programs for generating
Dart and Go code from Protocol Buffers specifications.  
See [the Makefile in timeterm/proto](https://gitlab.com/timeterm/proto/blob/master/Makefile)
for example usage of this container.