FROM golang:1.13

ENV PATH="/usr/lib/dart/bin:/root/.pub-cache/bin:${PATH}"

RUN apt-get update && apt-get install -y apt-transport-https unzip \
 && sh -c 'curl https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -' \
 && sh -c 'curl https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list' \
 && apt-get update && apt-get install -y dart \
 && curl -fsSL -o protoc.zip https://github.com/protocolbuffers/protobuf/releases/download/v3.10.0/protoc-3.10.0-linux-x86_64.zip \
 && unzip -o protoc.zip -d /usr bin/protoc \ 
 && unzip -o protoc.zip -d /usr include/* \
 && rm -f protoc.zip \
 && chmod +x /usr/bin/protoc \
 && pub global activate protoc_plugin \
 && go get github.com/gogo/protobuf/protoc-gen-gogoslick \
 && go get google.golang.org/grpc
